#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    //create file named "assignment9.txt"
    FILE *fp = fopen("assignment9.txt", "w");
    //write to file
    fprintf(fp, "UCSC is one of the leading institutes in Sri Lanka for computing studies.\n");
    //close file
    fclose(fp);

    //open file in read mode
    fp = fopen("assignment9.txt", "r");
    //readfile
    char c;
    while ((c = fgetc(fp)) != EOF)
    {
        printf("%c", c);
    }
    //close file
    fclose(fp);

    //open file in append mode
    fp = fopen("assignment9.txt", "a");
    //write to file
    fprintf(fp, "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.\n");
    //close file
    fclose(fp);

return 0;

}
